# ****************************** NoSQL ***********************************
#### Let's today discover about NoSQL.

As we know that before existing NoSQL we were working on SQL supported  data base, that is the **RDBMS**.
**Relational Database Management System (RDBMS)** is a data base management system, which supports the concept of Structured Query Language(SQL).

The term Relation intended to Table, that is in RDBMS the data is stored in the form of tables. In RDBMS, data is divided into multiple tables which are in the normalized form for achieving more efficiency.

For accessing the data from RDBMS we can use the **SQL**. However, The SQL supported data bases are good enough because of its simplicity and normal form of data bases and it's also supports the concept of **ACID**, generally known as 


-**A for Atomicity.**
-**C means Consistency.**
-**I represents the term Isolation.**
-**D is the abbreviation of Durability.**



Generally in real life this concept is used for mainly when we have advance known of structure of data bases, or Transaction related data bases, like Banking System, Railway Reservation Systems, and many more which supports transactions.


But, the problem with this data base is to it can not store the data which is unstructured, that means the data which has no predefined structure, we can also define like which has no any Schema. Like, in the real world scenario there are the real time applications whose data bases does not having any kind of structure.


**NoSQL**

The definition of NoSQL is hidden in the term NoSQL only, where N means Not, O means Only and SQL represents Structured Query Language, that means NoSQL represents Not Only SQL. It is also known as non-relational data base. It is a distributed data model that does not follow the concept of relational data base model. It supports huge data storage, because in today's era the data size is increasing like pollution. NoSQL are also horizontally scalable. It has an advantage of flexibility with huge unstructured data storage. Developers can easily store the data without any structure and easily fetch them.

The NoSQL model supported data base applications are generally also supports CAP theorem.

**CAP** is a theorem that follows three principles,Where C means Consistency, A represents Availability and P means Partition tolerance.

**Consistency :-** :The term consistent means that, the data available on all the distributed machines should be same in all forms and updating of data to be made on all distributed machines frequently.

**Availability :-** :The Availability means that, Data must be available permanently and should be accessible 24 x 7.

**Partition tolerance** :It means that, During machine failures or any faults in the machines, database going to work fine without stopping their work. For this we can keep replica to another server.

#### Characteristics Of NoSQL
- NoSQL databases stores huge amount of data.
- In distributed environment, we can easily use NoSQL without any inconsistency.
- If any failures occurs in any machine, then there will be no discontinuation of any work.Because of its replica.
- NoSQL allows us to store data in any format, it is not having any fixed schema.
- It is having more flexible structure.
- NoSQL does not use concept of ACID properties, unlike SQL.
- It is horizontally scalable leading to high performance.

### Now, we are moving towards the types of NoSQL data bases.


 **1. Document Oriented data bases:-** 
    These are the databases, which stores the data in the form of text that is unstructured or in semi-structured format. In this each documents contains pair of fields and values. Document Oriented databases are schema free and are not fixed in nature, that means it can be extendable by its schema as per requirements.
    The one of the example of this data base is MongoDB.

 **2. Key-value data bases:-**
    These data bases uses some key to store its data that is the value. It is much simpler to retrieve the data by using their key as reference.So for fetching the data in key value databases, we do not need to write much complex queries.
    Some of the examples of Key-Value data bases are, DynamoDB, Redis.  

 **3. Wide-column stores database:-**
    It store data in tables, rows, and dynamic columns. Wide-column stores databases provide much flexibility over SQL supported databases, because in this database there is not necessary that each row have the same columns. Some times it is also known as two dimensional Key-Value data base.
    The most popular databases which supports Wide-Column data bases are Cassandra and HBase.

 **4. Graph database:-**
    As the term Graph means we have some vertex that are connected with some edges, like the graph the graph data base stores the data in the form of vertex and edges.
    It is generally used in social networking applications and when we have to store the places and how they are connected.
    The popular Graph supported data bases are Neo4J, JanusGraph.


**The five NoSQL databases which i will suggest are  ----**

1. **MongoDB :-**
    MongoDB is one of the popular document-oriented NoSQL database. The Online MongoDB data base server is now MongoDB atlas.It is also replicable.
    Now a days it is most popular for many organizations and developers to store documents oriented data.

1. **ElasticSearch :-**
    This NoSQL database is used in case we required the full-text search, that means when searches occurs frequently. 
    Many companies using this data base like Medium, StackOverflow.


1. **DynamoDB :-**
    DynamoDB, is the known for its scalability.

1. **Neo4J :-**
    Neo4j is a database which support NoSQL and it is ‘graph-based database'.It is generally used for social networks and Maps.

1. **Apache Cassandra :-**
    It is an open source NoSQL database. Cassandra is a distributed database management system that is scalable. Cassandra having the  ability to manage large amounts of structured, semi-structured, and unstructured data.


**References :**
- [Research gate](https://www.researchgate.net/publication/323057709_NoSQL_databases_Critical_analysis_and_comparison)
- [MongoDB](https://www.mongodb.com/nosql-explained)

    **Thanks!**